---
title : Introduction à l'informatique
author : M. BODDAERT
license : CC-BY-NC-SA
---
# Introduction

## L'informatique c'est quoi ?

L'informatique est structurée par quatre concepts:

- L'__algorithme__ : méthode opérationnelle permettant de résoudre un problème.
- La __machine__ : système physique doté de fonctionnalités permettant de calculer.
- Le __langage__ : moyen de communication entre l'informaticien et la machine.
- Les __données__ : éléments symboliques susceptibles d'être traitées par une machine.

À ces concepts s'ajoute un élément transversal : Les __interfaces__

```mermaid
flowchart LR
	Donnée --> |Est utilisé|Algorithme
	Algorithme --> |Est Traduit|Langage
  Langage --> |Est exécuté|Machine
  Machine <--> |Manipule|Donnée
  Interface <--> |Commande|Machine
  Utilisateur <--> |Intéragit|Interface
  Interface <--> |Stocke|Donnée
```

« *L'informatique n'est pas plus la science des ordinateurs que l'astronomie n'est celle des télescopes* » (Michael R. Fellows et Ian Parberry)

## Définition

On peut dire que l'informatique est : __La science du traitement automatisé de l'information__.

- __Science__ : donc un aspect théorique (domaine des mathématiques)
- __Traitement automatisé__ : c'est l'ordinateur (domaine des technologies)
- __Information__ (au sens de [Claude Shannon](https://fr.wikipedia.org/wiki/Claude_Shannon)) : Grandeur mesurable, observable tout ce qui est numérisable (texte,musique, voix,image, films, ADN,...)

L'informatique regroupe un grand nombre de domaines : langage et programmation, architecture et système, théorie des graphes, réseau, sécurité, base de données, intelligence artificielle, etc.

Comme toute connaissance scientifique et technique, les concepts de l'informatique ont une __histoire__ plus ancienne qu'il n'y parait et ont été forgés par des femmes et des hommes...sur des siècles !

## Une brève histoire

| Qui | Date | Événement |
| :--: | :--: | :-- |
| - | 1er siècle avant J.-C. | Machine d’Anticythère, le plus vieux __mécanisme à engrenages connu__. |
| __Charles Babbage__ | 1821 | Commence à construire sa __machine à différences__, première machine mécanique programmable. |
| __Alan Turing__ | 1936 | Il présente le modèle des __machines de Turing__ et construit (mathématiquement) la première machine universelle. |
| - | 1943 | __ENIAC__, machine servant au calcul des trajectoires balistiques, est créée. Son poids est de 30 tonnes pour des dimensions de 2,4 x 0,9 x 30,5 mètres occupant une surface de 67 mètres carrés.![ENIAC](./assets/Eniac.jpeg) |
| __John Von Neumann__ | 1944 | A donné son nom à « l’architecture de __von Neumann__ » utilisée dans la quasi-totalité des ordinateurs modernes. |
| __DARPA__ | 1969| Début d'Arpanet, le prédécesseur d'Internet par le ministère de la défense américaine |
| __Intel__ | 1971| Le microprocesseur 4004 d’Intel est de la taille d’un timbre, il développe des performances équivalents à celle de l’ENIAC (1946), qui occupait toute une pièce.![Intel 4004](./assets/Intel_C4004.jpeg) |
| __Jimmy Wales__ | 2001 | Création de __Wikipédia__ |

### À Faire

- Cette histoire est très incomplète, vous allez contribuer à la développer.
- Via votre téléphone ou ordinateur, trouver les dates / protagonistes des événements suivants :

| Qui | Date | Événement / Fait |
| :--: | :--: | :--: |
| | | Premier réseau social sur Internet |
| | | Sortie du 1er Iphone |
| | | Premier site web |
| | | Création du premier ordinateur |
| | | Sortie du premier micro-ordinateur |
| | | Sortie du premier ordinateur portable |
| | | Création du langage Python |
| | | Création d'Internet |
| | | Création de la souris |
| | | Création du mot Bug |
| | | Premier programme informatique |
| | | Première machine à calculer |
| | | Création du CD-ROM |
| | | Première langage de programmation |
| | | Utilisation du système binaire |
| | | Création du mot ordinateur |
| | | Création du mot informatique |

- Pour vous aider, il est possible de consulter le site de Serge Rossi : [http://histoire.info.online.fr/](http://histoire.info.online.fr/)

